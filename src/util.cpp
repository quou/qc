#include "util.hpp"

namespace util {
	usize hash(const u8* data, usize size) {
		usize hash = 0, x = 0;

		for (usize i = 0; i < size; i++) {
			hash = (hash << 4) + data[i];
			if ((x = hash & 0xf000000000ll) != 0) {
				hash ^= (x >> 24);
				hash &= ~x;
			}
		}

		return (hash & 0x7fffffffff);
	}

	usize hash_string(const std::string& str) {
		return hash(reinterpret_cast<const u8*>(str.c_str()), str.size());
	}
}
