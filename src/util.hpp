#pragma once

#include <string>

#include "common.h"

namespace util {
	usize hash(const u8* data, usize size);
	usize hash_string(const std::string& str);
}
