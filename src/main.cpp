#include <fstream>
#include <iostream>

#include <errno.h>

#include "common.h"
#include "gen.hpp"
#include "lexer.hpp"
#include "parser.hpp"

i32 main(i32 argc, const char** argv) {
	if (argc < 2) {
		std::cerr << "Usage: " << argv[0] << " <filename>\n";
		return EINVAL;
	}

	char* source;

	std::string filename(argv[1]);

	std::fstream file(filename, std::ios::in | std::ios::binary);
	if (!file.good()) {
		return ENOENT;
	}

	file.seekg(0, file.end);
	usize size = file.tellg();
	file.seekg(0, file.beg);

	source = new char[size + 1];
	file.read(source, size);
	source[size] = '\0';

	file.close();

	Lexer lexer(filename, source);
	Parser parser(lexer);

	auto r = parser.parse();

	std::string outfilename = argc > 2 ? argv[2] : "a.o";
	
	Generator_LLVM generator(r);
	generator.generate(outfilename);

	/* std::fstream outfile("a.c", std::ios::out);
	if (!outfile.good()) {
		return ENOENT;
	}

	outfile.write(generator.result.c_str(), generator.result.size());
	outfile.close(); */

	delete[] source;

	return 0;
}
