#pragma once

#include "common.h"
#include "lexer.hpp"

namespace logger {
	void error(const char* src, const Token& token, const char* filename, u32 line, const char* fmt, ...);
}
