#pragma once

#include <string>
#include <cassert>

#include "common.h"
#include "util.hpp"

struct Value_Type {
	enum class Base_Type : u32 {
		integer,
		real,
		boolean,
		void_
	} base = Base_Type::void_;
	u32 size = 0;
	/* The depth of a pointer to this type.
	 *
	 * 1 = *
	 * 2 = **
	 * 3 = **
	 * etc.
	 */
	i32 ptr = 0;

	bool is_signed = false;

	char padding[3] {};

	Value_Type derefs_to() const {
		assert(ptr > 0 && "Cannot deref a non-pointer type.");

		Value_Type n = *this;
		n.ptr--;
		return n;
	}

	bool is_ptr() const {
		return ptr > 0;
	}

	bool is_numeric() const {
		return base == Base_Type::real || base == Base_Type::integer;
	}

	static bool valid(const std::string& name);
	static Value_Type make(const std::string& name);
	static bool binary_compat(const Value_Type& lhs, const Value_Type& rhs);

	constexpr static Value_Type make_f32() {
		return Value_Type {
			.base = Value_Type::Base_Type::real,
			.size = 4,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_f64() {
		return Value_Type {
			.base = Value_Type::Base_Type::real,
			.size = 8,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_i8() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 1,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_u8() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 1,
			.is_signed = false
		};
	}

	constexpr static Value_Type make_i16() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 2,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_u16() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 2,
			.is_signed = false
		};
	}

	constexpr static Value_Type make_i32() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 4,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_i64() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 8,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_u32() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 4,
			.is_signed = false
		};
	}

	constexpr static Value_Type make_u64() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 8,
			.is_signed = false
		};
	}

	constexpr static Value_Type make_bool() {
		return Value_Type {
			.base = Value_Type::Base_Type::boolean,
			.size = 1
		};
	}

	constexpr static Value_Type make_char_ptr() {
		return Value_Type {
			.base = Value_Type::Base_Type::integer,
			.size = 1,
			.ptr = 1,
			.is_signed = true
		};
	}

	constexpr static Value_Type make_void() {
		return Value_Type {
			.base = Value_Type::Base_Type::void_
		};
	}

	bool operator==(const Value_Type& other) const {
		return base == other.base && size == other.size && is_signed == other.is_signed && ptr == other.ptr;
	}

	bool operator!=(const Value_Type& other) const {
		return !(*this == other);
	}

	static bool castable(const Value_Type& from, const Value_Type& to);

	struct Hasher {
		usize operator()(const Value_Type& v) const noexcept {
			const u8* data = reinterpret_cast<const u8*>(&v);

			return util::hash(data, sizeof v);
		}
	};
};
