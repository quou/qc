#include <string>
#include <cstdio>
#include <cstdarg>
#include <cstring>

#include "logger.hpp"

namespace logger {
	void error(const char* src, const Token& token, const char* filename, u32 line, const char* fmt, ...) {
		const char* line_start = token.start - 1;
		while (line_start != src && *line_start != '\n') {
			line_start--;
		}

		if (*line_start == '\n') { line_start++; }

		i32 line_len = 0;
		for (const char* c = line_start; *c && *c != '\n'; c++, line_len++) {}

		u32 col = 1;
		for (const char* c = line_start; *c && c != token.start; c++, col++) {}

		va_list args;
		va_start(args, fmt);

		std::printf("%s:%u:%u: \033[31;1;31merror\033[0m: ", filename, line, col);
		std::printf("\033[1m");
		std::vprintf(fmt, args);
		std::printf("\033[0m");
		std::printf("\n");

		std::printf("%.*s\n", line_len, line_start);

		std::printf("\033[1;32m");

		for (u32 i = 0; i < col - 1; i++) {
			if (line_start[i] == '\t') {
				std::printf("\t");
			} else {
				std::printf(" ");
			}
		}

		std::printf("^");

		for (u32 i = 0; i < token.len - 1; i++) {
			std::printf("~");
		}

		std::printf("\033[0m\n");

		va_end(args);
	}
}
