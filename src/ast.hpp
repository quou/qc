#pragma once

#include <memory>
#include <vector>

#include "common.h"
#include "lexer.hpp"
#include "type.hpp"

/* Abstract Syntax Tree. */

class Generator_C;
class Generator_LLVM;
class Parser;

namespace llvm {
	class Value;
	class Function;
	class BasicBlock;
};

struct Code_Gen_Result {
	union {
		llvm::Value* value;
		llvm::Function* function;
	};

	enum class Type {
		value,
		function
	} type;
};

namespace AST {
	class Namespace;

	namespace Nodes {
		class Statement {
		protected:
			Token token;
		public:
			Statement(const Token& token) : token(token) {}
			virtual ~Statement() {}

			/* Statements can return null here, since they don't always
			 * produce a value. Expression must produce a value, however. */
			virtual llvm::Value* gen_llvm(Generator_LLVM& gen) = 0;

			inline const Token& get_token() const {
				return token;
			}
		};

		class Expression : public Statement {
		protected:
			Value_Type returns;
		public:
			virtual ~Expression() {}

			Expression(const Token& token, Value_Type returns)
				: Statement(token), returns(returns) {}

			inline Value_Type get_returns() const {
				return returns;
			}
		};

		class Number : public Expression {
		private:
			union {
				i32 i32_val;
				i64 i64_val;
				u32 u32_val;
				u64 u64_val;
				f64 f64_val;
				f32 f32_val;
			};
		public:
			Number(const Token& token, i32 v) : Expression(token, Value_Type::make_i32()), i32_val(v) {}
			Number(const Token& token, i64 v) : Expression(token, Value_Type::make_i64()), i64_val(v) {}
			Number(const Token& token, u32 v) : Expression(token, Value_Type::make_u32()), u32_val(v) {}
			Number(const Token& token, u64 v) : Expression(token, Value_Type::make_u64()), u64_val(v) {}
			Number(const Token& token, f32 v) : Expression(token, Value_Type::make_f32()), f32_val(v) {}
			Number(const Token& token, f64 v) : Expression(token, Value_Type::make_f64()), f64_val(v) {}

			inline i32 as_i32() const {
				return i32_val;
			}

			inline i64 as_i64() const {
				return i64_val;
			}

			inline u32 as_u32() const {
				return u32_val;
			}

			inline u64 as_u64() const {
				return u64_val;
			}

			inline f32 as_f32() const {
				return f32_val;
			}

			inline f64 as_f64() const {
				return f64_val;
			}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class String : public Expression {
		private:
			std::string val;
		public:
			String(const Token& token, const std::string& v) : Expression(token, Value_Type::make_char_ptr()), val(v) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Boolean : public Expression {
		private:
			bool val;
		public:
			Boolean(const Token& token, bool v) : Expression(token, Value_Type::make_bool()), val(v) {}

			bool get_val() const {
				return val;
			}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Binary_Operation : public Expression {
		public:
			enum class Type {
				add,
				subtract,
				multiply,
				divide,
				mod,
				assign,
				equal,
				not_equal,
				less,
				less_equal,
				greater,
				greater_equal,
				or_,
				and_
			};
		private:
			Type type;

			std::unique_ptr<Expression> lhs;
			std::unique_ptr<Expression> rhs;
		public:
			Binary_Operation(const Token& token, Type type, Value_Type returns, std::unique_ptr<Expression> lhs, std::unique_ptr<Expression> rhs) :
				Expression(token, returns), type(type), lhs(std::move(lhs)), rhs(std::move(rhs)) {}

			inline Type get_type() const {
				return type;
			}

			inline bool ptr_compat() const {
				return type == Type::add;
			}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Unary_Operation : public Expression {
		public:
			enum class Type {
				negate,
				reference,
				dereference,
				bang
			};
		private:
			Type type;

			std::unique_ptr<Expression> expr;
		public:
			Unary_Operation(const Token& token, Type type, Value_Type returns, std::unique_ptr<Expression> expr)
				: Expression(token, returns), type(type), expr(std::move(expr)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Variable : public Expression {
		private:
			std::string name;
		public:
			Variable(const Token& token, Value_Type type, const std::string& name) : Expression(token, type), name(name) {}

			inline const std::string& get_name() const {
				return name;
			}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Call : public Expression {
		private:
			std::string name;
			std::vector<std::unique_ptr<Expression>> args;

			std::shared_ptr<Namespace> ns;
		public:
			Call(const Token& token, const std::string& name, Value_Type returns, std::vector<std::unique_ptr<Expression>> args,
				const std::shared_ptr<Namespace>& ns = null)
				: Expression(token, returns), ns(ns), name(name), args(std::move(args)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;

			const std::shared_ptr<Namespace>& get_namespace() {
				return ns;
			}
		};

		class Return : public Statement {
		private:
			std::unique_ptr<Expression> expr;
		public:
			Return(const Token& token, std::unique_ptr<Expression> expr)
				: Statement(token), expr(std::move(expr)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Block : public Statement {
		private:
			std::vector<std::unique_ptr<Nodes::Statement>> statements;

			bool has_return;
		public:
			Block(const Token& token, std::vector<std::unique_ptr<Nodes::Statement>> statements, bool has_return)
				: Statement(token), statements(std::move(statements)), has_return(has_return) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;

			inline bool get_has_return() const {
				return has_return;
			}
		};

		class If : public Statement {
		private:
			std::unique_ptr<Expression> cond;
			std::unique_ptr<Block> then;
			std::unique_ptr<Statement> else_;
		public:
			If(const Token& token, std::unique_ptr<Expression> cond, std::unique_ptr<Block> then, std::unique_ptr<Statement> else_ = null)
				: Statement(token), cond(std::move(cond)), then(std::move(then)), else_(else_ ? std::move(else_) : null) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;

			bool then_has_return() {
				return then->get_has_return();
			}

			/* Recursively traverses down the elif chain to see if any of them return a value. */
			bool else_has_return() {
				if (!else_) { return false; }

				auto block = dynamic_cast<AST::Nodes::Block*>(else_.get());
				if (block) {
					return block->get_has_return();
				}

				auto elif  = dynamic_cast<AST::Nodes::If*>(else_.get());
				if (elif) {
					return elif->else_has_return();
				}

				return false;
			}
		};

		class Loop : public Statement {
		private:
			std::unique_ptr<Block> body;
		public:
			Loop(const Token& token, std::unique_ptr<Block> body)
				: Statement(token), body(std::move(body)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class While : public Statement {
		private:
			std::unique_ptr<Expression> cond;
			std::unique_ptr<Block> body;
		public:
			While(const Token& token, std::unique_ptr<Expression> cond, std::unique_ptr<Block> body)
				: Statement(token), cond(std::move(cond)), body(std::move(body)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class For : public Statement {
		private:
			std::unique_ptr<Statement>  decl;
			std::unique_ptr<Expression> cond;
			std::unique_ptr<Expression> incr;
			std::unique_ptr<Block> body;
		public:
			For(const Token& token, std::unique_ptr<Statement> decl, std::unique_ptr<Expression> cond, std::unique_ptr<Expression> incr,
				std::unique_ptr<Block> body)
				: Statement(token), decl(std::move(decl)), cond(std::move(cond)), incr(std::move(incr)), body(std::move(body)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Break : public Statement {
		public:
			Break(const Token& token)
				: Statement(token) {}
			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Continue : public Statement {
		public:
			Continue(const Token& token)
				: Statement(token) {}
			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Variable_Declaration : public Statement {
		private:
			std::string name;

			Value_Type vtype;

			std::unique_ptr<Nodes::Expression> initialiser;
		public:
			Variable_Declaration(const Token& token, const std::string& name, Value_Type vtype, std::unique_ptr<Nodes::Expression> initialiser)
				: Statement(token), name(name), initialiser(std::move(initialiser)), vtype(vtype) {}

			const std::string& get_name() const {
				return name;
			}

			const Value_Type& get_vtype() const {
				return vtype;
			}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};

		class Cast : public Expression {
		private:
			std::unique_ptr<Nodes::Expression> expr;
		public:
			Cast(const Token& token, const Value_Type& to, std::unique_ptr<Nodes::Expression> expr)
				: Expression(token, to), expr(std::move(expr)) {}

			llvm::Value* gen_llvm(Generator_LLVM& gen) override;
		};
	}

	struct Argument {
		std::string name;
		Value_Type type;
		Token token;
	};

	class Declaration {
	public:
		enum class Type {
			procedure,
			prototype
		};
	protected:
		Type type;
		Value_Type vtype;

		friend class Procedure;

		Declaration(Type type, Value_Type vtype)
			: type(type), vtype(vtype) {}
	public:
		virtual ~Declaration() {}
		virtual Code_Gen_Result gen_llvm(Generator_LLVM& gen) = 0;

		Type get_type() const {
			return type;
		}

		Value_Type get_value_type() const {
			return vtype;
		}
	};

	/* Prototype for a procedure. */
	class Proc_Prototype : public Declaration {
	private:
		std::string name;
		std::vector<Argument> args;

		bool is_extern;
	public:
		Proc_Prototype(const std::string& name, Value_Type ret_type, std::vector<Argument> args, bool is_extern)
			: name(name), args(std::move(args)), is_extern(is_extern), Declaration(Declaration::Type::prototype, ret_type) {}

		inline const std::string& get_name() const {
			return name;
		}

		const std::vector<Argument>& get_args() {
			return args;
		}

		Code_Gen_Result gen_llvm(Generator_LLVM& gen) override;
	};

	class Procedure : public Declaration {
	private:
		std::unique_ptr<Proc_Prototype> prot;
		std::unique_ptr<Nodes::Block> body;

		std::shared_ptr<Namespace> ns;

		Token token;

		friend class Namespace;
	public:
		Procedure(const Token& token, std::unique_ptr<Proc_Prototype> prot, std::unique_ptr<Nodes::Block> body,
			const std::shared_ptr<Namespace>& ns = null)
			: Declaration(Declaration::Type::procedure, prot->vtype), token(token), prot(std::move(prot)), body(std::move(body)), ns(ns) {}

		Code_Gen_Result gen_llvm(Generator_LLVM& gen) override;

		const std::shared_ptr<Namespace>& get_namespace() {
			return ns;
		}
	};

	class Namespace {
	private:
		std::unordered_map<std::string, std::shared_ptr<Namespace>> subs;
		std::shared_ptr<Namespace> parent;

		std::vector<std::shared_ptr<Procedure>> procs;
		std::vector<std::shared_ptr<Proc_Prototype>> prots;

		std::string name;
		std::string mangled_name;

		friend class Generator_LLVM;
		friend class ::Parser;
	public:
		Namespace(const std::string& name, const std::shared_ptr<Namespace> parent = null) : name(name), parent(parent) {
			mangled_name = "_nmspc___" + std::to_string(util::hash_string(name)) + name + "___nmspc_";

			if (parent && parent->get_name() != "<global>") {
				mangled_name += parent->get_mangled_name();
			}
		}

		void add_sub(const std::shared_ptr<Namespace>& sub) {
			subs[sub->get_name()] = sub;
		}

		void add_proc(const std::shared_ptr<AST::Procedure>& proc) {
			procs.push_back(proc);
		}

		void add_prot(const std::shared_ptr<AST::Proc_Prototype>& prot) {
			prots.push_back(prot);
		}

		const std::string& get_name() const {
			return name;
		}

		const std::string& get_mangled_name() const {
			return mangled_name;
		}
		
		std::string get_abs_name() const {
			std::string n;

			if (parent) {
				n += parent->get_abs_name() + ":";
			}

			n += name;

			return n;
		}

		std::shared_ptr<Namespace> get_sub(const std::string& name) {
			if (subs.count(name) == 0) {
				return null;
			}

			return subs[name];
		}

		bool has_sub(const std::string& name) {
			return subs.count(name) != 0;
		}

		void generate_prots(Generator_LLVM& gen);
		void generate_procs(Generator_LLVM& gen);
	};
};
