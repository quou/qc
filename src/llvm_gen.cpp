#include <string>
#include <iostream>
#include <cassert>

#include <llvm/ADT/APFloat.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Verifier.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Target/TargetOptions.h>

#include "ast.hpp"
#include "gen.hpp"
#include "logger.hpp"

llvm::Type* Generator_LLVM::gen_type(Value_Type type) {
	if (type.ptr > 0) {
		Value_Type nt = type;
		nt.ptr--;
		return llvm::PointerType::get(gen_type(nt), 0);
	}

	switch (type.base) {
		case Value_Type::Base_Type::integer:
			switch (type.size) {
				case 1: return llvm::Type::getInt8Ty(ctx);
				case 2: return llvm::Type::getInt16Ty(ctx);
				case 4: return llvm::Type::getInt32Ty(ctx);
				case 8: return llvm::Type::getInt64Ty(ctx);
			}
			break;
		case Value_Type::Base_Type::real:
			switch (type.size) {
				case 8: return llvm::Type::getDoubleTy(ctx);
				case 4: return llvm::Type::getFloatTy(ctx);
			}
			break;
		case Value_Type::Base_Type::boolean:
			return llvm::Type::getInt1Ty(ctx);
		case Value_Type::Base_Type::void_:
			return llvm::Type::getVoidTy(ctx);
		default: break;
	}

	return null;
}

llvm::AllocaInst* Generator_LLVM::create_entry_alloca(llvm::Function* fun, const Value_Type& type, const std::string& name) {
	llvm::IRBuilder<> t(&fun->getEntryBlock(), fun->getEntryBlock().begin());
	return t.CreateAlloca(gen_type(type));
}

bool Generator_LLVM::var_declared(const std::string& name) {
	for (const auto& map : vars) {
		if (map.count(name) != 0) {
			return true;
		}
	}
	return false;
}

void Generator_LLVM::declare_var(const Token& tok, const std::string& name, llvm::AllocaInst* dat) {
	if (vars.size() == 0) {
		fail(tok, "A scope must be defined to declare variables.");
	}

	if (var_declared(name)) {
		fail(tok, "Variable redeclared.");
	}

	auto& top = vars[vars.size() - 1];
	top[name] = dat;
}

llvm::AllocaInst* Generator_LLVM::get_var(const Token& tok, const std::string& name) {
	for (auto& map : vars) {
		if (map.count(name) != 0) {
			return map[name];
		}
	}

	fail(tok, "Undefined variable.");
	return null;
}

void Generator_LLVM::begin_scope() {
	vars.push_back(std::unordered_map<std::string, llvm::AllocaInst*>());
}

void Generator_LLVM::end_scope() {
	vars.pop_back();
}

namespace AST {
	namespace Nodes {
		llvm::Value* Number::gen_llvm(Generator_LLVM& gen) {
			if (returns == Value_Type::make_i32()) {
				return llvm::ConstantInt::get(gen.ctx, llvm::APInt(32, as_i32(), true));
			} else if (returns == Value_Type::make_i64()) {
				return llvm::ConstantInt::get(gen.ctx, llvm::APInt(64, as_i64(), true));
			} else if (returns == Value_Type::make_u32()) {
				return llvm::ConstantInt::get(gen.ctx, llvm::APInt(32, as_u32(), false));
			} else if (returns == Value_Type::make_u64()) {
				return llvm::ConstantInt::get(gen.ctx, llvm::APInt(64, as_u64(), false));
			} else if (returns == Value_Type::make_f32()) {
				return llvm::ConstantFP::get(gen.ctx, llvm::APFloat(as_f32()));
			} else if (returns == Value_Type::make_f64()) {
				return llvm::ConstantFP::get(gen.ctx, llvm::APFloat(as_f64()));
			} else {
				std::abort();
			}
		}

		llvm::Value* String::gen_llvm(Generator_LLVM& gen) {
			return gen.builder->CreateGlobalStringPtr(llvm::StringRef(val.c_str()));
		}

		llvm::Value* Boolean::gen_llvm(Generator_LLVM& gen) {
			return llvm::ConstantInt::get(gen.ctx, llvm::APInt(1, static_cast<u8>(val), false));
		}

		llvm::Value* Binary_Operation::gen_llvm(Generator_LLVM& gen) {
			Value_Type l_t = lhs->get_returns();
			Value_Type r_t = rhs->get_returns();

			if (!Value_Type::binary_compat(l_t, r_t)) {
				gen.fail(this, "Types of operand are incompatible.");
			}

			if (type == Type::assign) {
				auto v = dynamic_cast<AST::Nodes::Variable*>(lhs.get());
				if (!v) {
					gen.fail(this, "Destination of '=' must be a variable.");
				}

				auto r = rhs->gen_llvm(gen);
				auto var = gen.get_var(token, v->get_name());
				if (!var) {
					gen.fail(this, "Unknown variable.");
				}
				return gen.builder->CreateStore(r, var);
			}

			auto l = lhs->gen_llvm(gen);
			auto r = rhs->gen_llvm(gen);

			if (l_t.ptr > 0) {
				/* Pointer arithmetic. */
				switch (type) {
					case Type::add:
						return gen.builder->CreateGEP(l, r);
					case Type::subtract:
						return gen.builder->CreateGEP(l, gen.builder->CreateNeg(r));
					default: break;
				}
			}

			switch (type) {
				case Type::add:
					switch (returns.base) {
						case Value_Type::Base_Type::integer:
							return gen.builder->CreateAdd(l, r);
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFAdd(l, r);
						default: break;
					}
				case Type::subtract:
					switch (returns.base) {
						case Value_Type::Base_Type::integer:
							return gen.builder->CreateSub(l, r);
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFSub(l, r);
						default: break;
					}
				case Type::multiply:
					switch (returns.base) {
						case Value_Type::Base_Type::integer:
							return gen.builder->CreateMul(l, r);
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFMul(l, r);
						default: break;
					}
				case Type::divide:
					switch (returns.base) {
						case Value_Type::Base_Type::integer:
							if (returns.is_signed) {
								return gen.builder->CreateSDiv(l, r);
							} else {
								return gen.builder->CreateUDiv(l, r);
							}
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFDiv(l, r);
						default: break;
					}
				case Type::mod:
					switch (returns.base) {
						case Value_Type::Base_Type::integer:
							if (returns.is_signed) {
								return gen.builder->CreateSRem(l, r);
							} else {
								return gen.builder->CreateURem(l, r);
							}
						default:
							gen.fail(this, "'%' can only be used on integer types.");
							break;
					}
				case Type::equal:
					switch (l_t.base) {
						case Value_Type::Base_Type::integer:
						case Value_Type::Base_Type::boolean:
							return gen.builder->CreateICmpEQ(l, r);
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFCmpOEQ(l, r);
						default: break;
					}
				case Type::not_equal:
					switch (l_t.base) {
						case Value_Type::Base_Type::integer:
						case Value_Type::Base_Type::boolean:
							return gen.builder->CreateNot(gen.builder->CreateICmpEQ(l, r));
						case Value_Type::Base_Type::real:
							return gen.builder->CreateNot(gen.builder->CreateFCmpOEQ(l, r));
						default: break;
					}
				case Type::less:
					switch (l_t.base) {
						case Value_Type::Base_Type::integer:
							if (l_t.is_signed) {
								return gen.builder->CreateICmpSLT(l, r);
							} else {
								return gen.builder->CreateICmpULT(l, r);
							}
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFCmpOLT(l, r);
						default: break;
					}
				case Type::less_equal:
					switch (l_t.base) {
						case Value_Type::Base_Type::integer:
							if (l_t.is_signed) {
								return gen.builder->CreateICmpSLE(l, r);
							} else {
								return gen.builder->CreateICmpULE(l, r);
							}
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFCmpOLE(l, r);
						default: break;
					}
				case Type::greater:
					switch (l_t.base) {
						case Value_Type::Base_Type::integer:
							if (l_t.is_signed) {
								return gen.builder->CreateICmpSGT(l, r);
							} else {
								return gen.builder->CreateICmpUGT(l, r);
							}
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFCmpOGT(l, r);
						default: break;
					}
				case Type::greater_equal:
					switch (l_t.base) {
						case Value_Type::Base_Type::integer:
							if (l_t.is_signed) {
								return gen.builder->CreateICmpSGE(l, r);
							} else {
								return gen.builder->CreateICmpUGE(l, r);
							}
						case Value_Type::Base_Type::real:
							return gen.builder->CreateFCmpOGE(l, r);
						default: break;
					}
				case Type::or_:
					return gen.builder->CreateOr(l, r);
				case Type::and_:
					return gen.builder->CreateAnd(l, r);
				default: break;
			}

			gen.fail(this, "Bad type.");
			return null;
		}

		llvm::Value* Unary_Operation::gen_llvm(Generator_LLVM& gen) {
			auto e = expr->gen_llvm(gen);

			switch (type) {
				case Type::negate:
					if (expr->get_returns().base == Value_Type::Base_Type::integer) {
						return gen.builder->CreateNeg(e, "negtmp");
					} else {
						return gen.builder->CreateFNeg(e, "negtmp");
					}
				case Type::reference: {
					AST::Nodes::Variable* var = dynamic_cast<AST::Nodes::Variable*>(expr.get());
					return gen.get_var(var->get_token(), var->get_name());
				}
				case Type::dereference: {
					return gen.builder->CreateLoad(gen.gen_type(expr->get_returns().derefs_to()), expr->gen_llvm(gen));
				}
				case Type::bang: {
					return gen.builder->CreateNot(expr->gen_llvm(gen));
				}
				default: break;
			}
		}

		llvm::Value* Variable::gen_llvm(Generator_LLVM& gen) {
			auto v = gen.get_var(token, name);
			if (!v) {
				gen.fail(this, "Unknown variable.");
			}

			return gen.builder->CreateLoad(v, name.c_str());
		}

		llvm::Value* Call::gen_llvm(Generator_LLVM& gen) {
			llvm::Function* f = gen.module->getFunction(name);
			if (!f) {
				std::cout << name << std::endl;
				gen.fail(this, "Unkown function.");
			}

			if (f->arg_size() != args.size()) {
				gen.fail(this, "Incorrect number of arguments.");
			}

			std::vector<llvm::Value*> vargs;
			for (const auto& a : args) {
				vargs.push_back(a->gen_llvm(gen));
			}

			return gen.builder->CreateCall(f, vargs);
		}

		llvm::Value* Return::gen_llvm(Generator_LLVM& gen) {
			if (!expr) {
				gen.builder->CreateRetVoid();
			} else {
				gen.builder->CreateRet(expr->gen_llvm(gen));
			}

			return null;
		}

		llvm::Value* If::gen_llvm(Generator_LLVM& gen) {
			auto cv = cond->gen_llvm(gen);

			auto one = llvm::ConstantInt::get(gen.ctx, llvm::APInt(1, 1, false));

			cv = gen.builder->CreateICmpEQ(cv, one);

			auto then_block = llvm::BasicBlock::Create(gen.ctx, "then", gen.current_fun);
			auto cont_block = llvm::BasicBlock::Create(gen.ctx, "cont");
			llvm::BasicBlock* else_block;
			if (else_) {
				else_block = llvm::BasicBlock::Create(gen.ctx, "else", gen.current_fun);
			}

			gen.builder->CreateCondBr(cv, then_block, else_ ? else_block : cont_block);

			/* Generate the "then" block. */
			gen.builder->SetInsertPoint(then_block);
			then->gen_llvm(gen);

			if (!then_has_return()) {
				gen.builder->CreateBr(cont_block);
			}

			/* Generate the "else" block. */
			if (else_) {
				gen.builder->SetInsertPoint(else_block);
				else_->gen_llvm(gen);

				if (!else_has_return()) {
					gen.builder->CreateBr(cont_block);
				}
			}

			then_block = gen.builder->GetInsertBlock();

			if (!else_has_return()) {
				gen.current_fun->getBasicBlockList().push_back(cont_block);
			}

			gen.builder->SetInsertPoint(cont_block);

			return null;
		}

		llvm::Value* Loop::gen_llvm(Generator_LLVM& gen) {
			auto bb   = llvm::BasicBlock::Create(gen.ctx, "loopbody", gen.current_fun);

			gen.builder->CreateBr(bb);

			auto cont = llvm::BasicBlock::Create(gen.ctx, "loopcont", gen.current_fun);

			gen.loopconts.push(cont);
			gen.loopstarts.push(bb);

			gen.builder->SetInsertPoint(bb);

			body->gen_llvm(gen);

			gen.loopconts.pop();
			gen.loopstarts.pop();

			if (!body->get_has_return()) {
				gen.builder->CreateBr(bb);
			}

			gen.builder->SetInsertPoint(cont);

			return null;
		}

		llvm::Value* Break::gen_llvm(Generator_LLVM& gen) {
			if (gen.loopconts.size() == 0) {
				gen.fail(this, "'break' must be in the body of a loop.");
			}

			gen.builder->CreateBr(gen.loopconts.top());
			return null;
		}

		llvm::Value* Continue::gen_llvm(Generator_LLVM& gen) {
			if (gen.loopstarts.size() == 0) {
				gen.fail(this, "'continue' must be in the body of a loop.");
			}

			gen.builder->CreateBr(gen.loopstarts.top());

			return null;
		}

		llvm::Value* While::gen_llvm(Generator_LLVM& gen) {
			auto one = llvm::ConstantInt::get(gen.ctx, llvm::APInt(1, 1, false));

			auto bb = llvm::BasicBlock::Create(gen.ctx, "whilebody", gen.current_fun);
			auto condb = llvm::BasicBlock::Create(gen.ctx, "whilecond", gen.current_fun);
			auto cont = llvm::BasicBlock::Create(gen.ctx, "whilecont", gen.current_fun);

			gen.loopstarts.push(condb);
			gen.loopconts.push(cont);

			gen.builder->CreateBr(condb);

			gen.builder->SetInsertPoint(condb);
			auto cv = gen.builder->CreateICmpEQ(cond->gen_llvm(gen), one);
			gen.builder->CreateCondBr(cv, bb, cont);

			gen.builder->SetInsertPoint(bb);

			body->gen_llvm(gen);

			gen.loopconts.pop();
			gen.loopstarts.pop();

			if (!body->get_has_return()) {
				gen.builder->CreateBr(condb);
			}

			gen.builder->SetInsertPoint(cont);

			return null;
		}

		llvm::Value* For::gen_llvm(Generator_LLVM& gen) {
			auto one = llvm::ConstantInt::get(gen.ctx, llvm::APInt(1, 1, false));

			auto bodyb = llvm::BasicBlock::Create(gen.ctx, "forbody", gen.current_fun);
			auto condb = llvm::BasicBlock::Create(gen.ctx, "forcond", gen.current_fun);
			auto incrb = llvm::BasicBlock::Create(gen.ctx, "forincr", gen.current_fun);
			auto cont  = llvm::BasicBlock::Create(gen.ctx, "forcont", gen.current_fun);

			decl->gen_llvm(gen);

			gen.loopstarts.push(condb);
			gen.loopconts.push(cont);

			gen.builder->CreateBr(condb);

			gen.builder->SetInsertPoint(incrb);
			incr->gen_llvm(gen);
			gen.builder->CreateBr(condb);

			gen.builder->SetInsertPoint(condb);
			auto cv = gen.builder->CreateICmpEQ(cond->gen_llvm(gen), one);
			gen.builder->CreateCondBr(cv, bodyb, cont);

			gen.builder->SetInsertPoint(bodyb);

			body->gen_llvm(gen);

			if (!body->get_has_return()) {
				gen.builder->CreateBr(incrb);
			}

			gen.loopconts.pop();
			gen.loopstarts.pop();

			gen.builder->SetInsertPoint(cont);

			return null;
		}

		llvm::Value* Block::gen_llvm(Generator_LLVM& gen) {
			for (auto& stmt : statements) {
				stmt->gen_llvm(gen);

				auto sub = dynamic_cast<AST::Nodes::Block*>(stmt.get());
				if (sub && sub->has_return) {
					break;
				}

				auto ret = dynamic_cast<AST::Nodes::Return*>(stmt.get());
				if (ret) {
					break;
				}
			}

			return null;
		}

		llvm::Value* Variable_Declaration::gen_llvm(Generator_LLVM& gen) {
			llvm::AllocaInst* a = gen.create_entry_alloca(gen.current_fun, vtype, name);

			if (initialiser) {
				gen.builder->CreateStore(initialiser->gen_llvm(gen), a);
			}

			gen.declare_var(token, name, a);

			return null;
		}

		llvm::Value* Cast::gen_llvm(Generator_LLVM& gen) {
			llvm::Instruction::CastOps inst;

			const Value_Type& from = expr->get_returns();
			const Value_Type& to   = get_returns();

			if (to.ptr > 0) {
				if (from.ptr == 0) {
					gen.fail(this, "Invalid cast.");
				}

				return gen.builder->CreatePointerCast(expr->gen_llvm(gen), gen.gen_type(to));
			}

			switch (to.base) {
				case Value_Type::Base_Type::integer:
					switch (from.base) {
						case Value_Type::Base_Type::real:
							if (get_returns().is_signed) {
								inst = llvm::Instruction::FPToSI;
							} else {
								inst = llvm::Instruction::FPToUI;
							}
							break;
						case Value_Type::Base_Type::integer:
							if (to.size != from.size) {
								return gen.builder->CreateZExtOrTrunc(expr->gen_llvm(gen), gen.gen_type(to));
							} else {
								return expr->gen_llvm(gen);
							}
							break;
						default:
							gen.fail(this, "Invalid cast.");
							break;
					}
					break;
				case Value_Type::Base_Type::real:
					switch (from.base) {
						case Value_Type::Base_Type::integer:
							if (expr->get_returns().is_signed) {
								inst = llvm::Instruction::SIToFP;
							} else {
								inst = llvm::Instruction::UIToFP;
							}
							break;
						case Value_Type::Base_Type::real:
							if (to.size < from.size) {
								return gen.builder->CreateFPTrunc(expr->gen_llvm(gen), gen.gen_type(to));
							} else if (to.size > from.size) {
								return gen.builder->CreateFPExt(expr->gen_llvm(gen), gen.gen_type(to));
							} else {
								return expr->gen_llvm(gen);
							}
							break;
						default:
							gen.fail(this, "Invalid cast.");
							break;
					}
					break;
				default:
					gen.fail(this, "Invalid cast.");
			}

			return gen.builder->CreateCast(inst, expr->gen_llvm(gen), gen.gen_type(to));
		}
	}

	Code_Gen_Result Proc_Prototype::gen_llvm(Generator_LLVM& gen) {
		std::vector<llvm::Type*> arg_types;
		arg_types.reserve(args.size());

		std::cout << name << std::endl;

		for (const auto& a : args) {
			arg_types.push_back(gen.gen_type(a.type));
		}

		auto ft = llvm::FunctionType::get(gen.gen_type(vtype), arg_types, false);

		auto f = llvm::Function::Create(ft, llvm::Function::ExternalLinkage, name, gen.module.get());

		usize i = 0;
		for (auto& a : f->args()) {
			a.setName(args[i++].name);
		}

		return Code_Gen_Result {
			.function = f,
			.type = Code_Gen_Result::Type::function
		};
	}

	Code_Gen_Result Procedure::gen_llvm(Generator_LLVM& gen) {
		llvm::Function* fun = gen.module->getFunction(prot->get_name());

		if (!fun) {
			fun = prot->gen_llvm(gen).function;
		}

		if (!fun) {
			gen.fail(token, "Failed to find function.");
		}

		gen.current_fun = fun;

		llvm::BasicBlock* b = llvm::BasicBlock::Create(gen.ctx, "entry", fun);
		gen.builder->SetInsertPoint(b);

		gen.begin_scope();

		usize i = 0;
		for (auto& arg : fun->args()) {
			llvm::AllocaInst* a = gen.create_entry_alloca(fun, prot->get_args()[i].type, arg.getName().data());

			gen.builder->CreateStore(&arg, a);

			gen.declare_var(prot->get_args()[i].token, arg.getName().data(), a);

			i++;
		}

		body->gen_llvm(gen);

		gen.end_scope();

		if (!body->get_has_return()) {
			if (vtype == Value_Type::make_void()) {
				gen.builder->CreateRetVoid();
			} else {
				gen.fail(token, "Procedure must return a value.");
			}
		}

		if (llvm::verifyFunction(*fun, &llvm::errs())) {
			std::cout << "Function verification failed.\n";
			std::abort();
		}

		return Code_Gen_Result {
			.function = fun,
			.type = Code_Gen_Result::Type::function
		};
	}

	void Namespace::generate_prots(Generator_LLVM& gen) {
		for (const auto& prot : prots) {
			prot->gen_llvm(gen);
		}

		for (const auto& proc : procs) {
			proc->prot->gen_llvm(gen);
		}

		for (const auto& n : subs) {
			n.second->generate_prots(gen);
		}
	}

	void Namespace::generate_procs(Generator_LLVM& gen) {
		for (const auto& proc : procs) {
			proc->gen_llvm(gen);
		}

		for (const auto& n : subs) {
			n.second->generate_procs(gen);
		}
	}
}

Generator_LLVM::Generator_LLVM(const std::shared_ptr<ParseResult>& parse_result)
	: parse_result(parse_result) {
	builder = std::make_unique<llvm::IRBuilder<>>(ctx);
	module  = std::make_unique<llvm::Module>(parse_result->get_module_name(), ctx);
}

void Generator_LLVM::fail(const AST::Nodes::Statement* stmt, const char* message) {
	fail(stmt->get_token(), message);
}

void Generator_LLVM::fail(const Token& tok, const char* message) {
	logger::error(parse_result->source, tok, parse_result->module_name.c_str(), tok.line, message);
	abort();
}

void Generator_LLVM::generate(const std::string& outfilename) {
	parse_result->ns->generate_prots(*this);
	parse_result->ns->generate_procs(*this);

	std::cout << " == Begin LLVM IR Dump ==" << std::endl;
	module->print(llvm::errs(), null);
	std::cout << " == End LLVM IR Dump ==" << std::endl;

	/* Write to an object file. */
	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargets();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllAsmPrinters();

	auto target_triple = llvm::sys::getDefaultTargetTriple();
	module->setTargetTriple(target_triple);

	std::string error;
	auto target = llvm::TargetRegistry::lookupTarget(target_triple, error);
	if (!target) {
		llvm::errs() << error;
		return;
	}

	const char* cpu = "generic";
	const char* features = "";

	llvm::TargetOptions opts;
	auto rm = llvm::Optional<llvm::Reloc::Model>(llvm::Reloc::Model::PIC_);
	auto target_machine = target->createTargetMachine(target_triple, cpu, features, opts, rm);

	module->setDataLayout(target_machine->createDataLayout());

	std::error_code error_code;
	llvm::raw_fd_ostream dst(outfilename, error_code, llvm::sys::fs::OF_None);

	if (error_code) {
		llvm::errs() << "Could not open file: " << error_code.message();
	}

	llvm::legacy::PassManager pass;
	auto file_type = llvm::CGFT_ObjectFile;

	if (target_machine->addPassesToEmitFile(pass, dst, null, file_type)) {
		llvm::errs() << "The target machine can't emit a file of this type.";
		return;
	}

	pass.run(*module);
	dst.flush();

	std::cout << "Written '" << outfilename << "'." << std::endl;
}
