#include <algorithm>
#include <unordered_map>

#include "type.hpp"

/* TODO: More integer types. */
std::unordered_map<std::string, Value_Type> primitive_names = {
	{ "int",   Value_Type::make_i64()  },
	{ "uint",  Value_Type::make_u64()  },
	{ "float", Value_Type::make_f64()  },
	{ "i8",    Value_Type::make_i8()   },
	{ "u8",    Value_Type::make_u8()   },
	{ "i16",   Value_Type::make_i16()  },
	{ "u16",   Value_Type::make_u16()  },
	{ "i32",   Value_Type::make_i32()  },
	{ "f32",   Value_Type::make_f32()  },
	{ "u32",   Value_Type::make_u32()  },
	{ "i64",   Value_Type::make_i64()  },
	{ "u64",   Value_Type::make_u64()  },
	{ "f64",   Value_Type::make_f64()  },
	{ "bool",  Value_Type::make_bool() },
	{ "usize", Value_Type::make_u64()  },
	{ "char",  Value_Type::make_i8()   },
	{ "byte",  Value_Type::make_u8()   },
	{ "void",  Value_Type::make_void() }
};

std::unordered_map<Value_Type, std::vector<Value_Type>, Value_Type::Hasher> compat = {
	{
		Value_Type::make_f64(),
		{
			Value_Type::make_i8(),
			Value_Type::make_i16(),
			Value_Type::make_u8(),
			Value_Type::make_u16(),
			Value_Type::make_i32(),
			Value_Type::make_i64(),
			Value_Type::make_u32(),
			Value_Type::make_u64(),
			Value_Type::make_f32(),
			Value_Type::make_f64(),
		}
	},
	{
		Value_Type::make_f32(),
		{
			Value_Type::make_i8(),
			Value_Type::make_i16(),
			Value_Type::make_u8(),
			Value_Type::make_u16(),
			Value_Type::make_i32(),
			Value_Type::make_i64(),
			Value_Type::make_u32(),
			Value_Type::make_u64(),
			Value_Type::make_f32(),
			Value_Type::make_f64(),
		}
	},
	{
		Value_Type::make_i64(),
		{
			Value_Type::make_i8(),
			Value_Type::make_i16(),
			Value_Type::make_u8(),
			Value_Type::make_u16(),
			Value_Type::make_i32(),
			Value_Type::make_i64(),
			Value_Type::make_u32(),
			Value_Type::make_u64(),
			Value_Type::make_f32(),
			Value_Type::make_f64(),
		}
	},
	{
		Value_Type::make_i32(),
		{
			Value_Type::make_i8(),
			Value_Type::make_i16(),
			Value_Type::make_u8(),
			Value_Type::make_u16(),
			Value_Type::make_i32(),
			Value_Type::make_i64(),
			Value_Type::make_u32(),
			Value_Type::make_u64(),
			Value_Type::make_f32(),
			Value_Type::make_f64(),
		}
	},
	{
		Value_Type::make_u64(),
		{
			Value_Type::make_i8(),
			Value_Type::make_i16(),
			Value_Type::make_u8(),
			Value_Type::make_u16(),
			Value_Type::make_i32(),
			Value_Type::make_i64(),
			Value_Type::make_u32(),
			Value_Type::make_u64(),
			Value_Type::make_f32(),
			Value_Type::make_f64(),
		}
	},
	{
		Value_Type::make_u32(),
		{
			Value_Type::make_i8(),
			Value_Type::make_i16(),
			Value_Type::make_u8(),
			Value_Type::make_u16(),
			Value_Type::make_i32(),
			Value_Type::make_i64(),
			Value_Type::make_u32(),
			Value_Type::make_u64(),
			Value_Type::make_f32(),
			Value_Type::make_f64(),
		}
	},
};

#include <iostream>

bool Value_Type::valid(const std::string& name) {
	return primitive_names.count(name) != 0;
}


Value_Type Value_Type::make(const std::string& name) {
	if (!valid(name)) { return make_void(); }
	return primitive_names[name];
}

bool Value_Type::binary_compat(const Value_Type& lhs, const Value_Type& rhs) {
	return
		(lhs == Value_Type::make_f64()  && rhs == Value_Type::make_f64())  || 
		(lhs == Value_Type::make_i64()  && rhs == Value_Type::make_i64())  || 
		(lhs == Value_Type::make_f32()  && rhs == Value_Type::make_f32())  || 
		(lhs == Value_Type::make_i32()  && rhs == Value_Type::make_i32())  || 
		(lhs == Value_Type::make_u32()  && rhs == Value_Type::make_u32())  || 
		(lhs == Value_Type::make_u64()  && rhs == Value_Type::make_u64())  || 
		(lhs == Value_Type::make_bool() && rhs == Value_Type::make_bool()) ||
		(lhs.ptr > 0                    && rhs == Value_Type::make_i64());
}

bool Value_Type::castable(const Value_Type& from, const Value_Type& to) {
	const auto& v = compat[from];
	return std::find(v.begin(), v.end(), to) != v.end();
}
