#include <fstream>
#include <iostream>
#include <unordered_map>

#include "parser.hpp"
#include "logger.hpp"

std::unordered_map<Token::Type, i32> binary_prec = {
	{ Token::Type::star,          7 },
	{ Token::Type::slash,         7 },
	{ Token::Type::percent,       7 },
	{ Token::Type::plus,          6 },
	{ Token::Type::minus,         6 },
	{ Token::Type::less,          5 },
	{ Token::Type::less_equal,    5 },
	{ Token::Type::greater,       5 },
	{ Token::Type::greater_equal, 5 },
	{ Token::Type::equal,         4 },
	{ Token::Type::bang_equal,    4 },
	{ Token::Type::and_,          3 },
	{ Token::Type::or_,           2 },
	{ Token::Type::assign,        1 }
};

static i32 get_precedence(Token::Type type) {
	if (binary_prec.count(type) == 0) {
		return -1;
	}

	return binary_prec[type];
}

static bool is_operator(Token::Type type) {
	return
		type == Token::Type::plus       ||
		type == Token::Type::minus      ||
		type == Token::Type::star       ||
		type == Token::Type::slash      ||
		type == Token::Type::assign     ||
		type == Token::Type::ampersand  ||
		type == Token::Type::bang       ||
		type == Token::Type::and_       ||
		type == Token::Type::or_        ||
		type == Token::Type::bang_equal ||
		type == Token::Type::equal      ||
		type == Token::Type::percent    ||
		type == Token::Type::less       ||
		type == Token::Type::less_equal ||
		type == Token::Type::greater    ||
		type == Token::Type::greater_equal;
}

static bool is_unary(Token::Type type) {
	return
		type == Token::Type::minus ||
		type == Token::Type::star ||
		type == Token::Type::ampersand ||
		type == Token::Type::bang;
}

Parser::Parser(Lexer& lexer) : lexer(lexer) {}

void Parser::fail_at(const char* message, const Token& token) {
	logger::error(lexer.source, token, lexer.module_name.c_str(), token.line, message);
	std::abort();
}

void Parser::fail_at_cur(const char* message) {
	fail_at(message, cur);
}

void Parser::fail_at_prev(const char* message) {
	fail_at(message, prev);
}

void Parser::fail(const char* message) {
	fail_at_prev(message);
}

bool Parser::var_declared(const std::string& name) {
	for (const auto& map : vars) {
		if (map.count(name) != 0) {
			return true;
		}
	}
	return false;
}

Value_Type Parser::get_var(const std::string& name) {
	for (auto& map : vars) {
		if (map.count(name) != 0) {
			return map[name];
		}
	}

	fail("Undefined variable.");
	return Value_Type { };
}

void Parser::declare_var(const std::string& name, Value_Type type) {
	if (vars.size() == 0) {
		fail("A scope must be defined to declare variables.");
	}

	if (var_declared(name)) {
		fail("Variable redeclared.");
	}

	auto& top = vars[vars.size() - 1];
	top[name] = type;
}

void Parser::begin_scope() {
	vars.push_back(std::unordered_map<std::string, Value_Type>());
}

void Parser::end_scope() {
	vars.pop_back();
}

void Parser::advance() {
	prev = cur;
	cur = lexer.next();
}

void Parser::expect(Token::Type type, const char* err) {
	if (cur.type != type) {
		fail_at_cur(err);
	}
	advance();
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_expression() {
	auto lhs = parse_unary();

	return parse_binary_rhs(0, std::move(lhs));
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_unary() {
	if (!is_operator(cur.type)) {
		return parse_primary();
	}

	AST::Nodes::Unary_Operation::Type type;

	auto op = cur;

	advance();

	auto expr = parse_expression();
	Value_Type returns;

	switch (op.type) {
		case Token::Type::minus:
			type = AST::Nodes::Unary_Operation::Type::negate;
			returns = expr->get_returns();

			if (!expr->get_returns().is_numeric()) {
				fail("Operand to '-' must be numeric.");
			}

			break;
		case Token::Type::ampersand: {
			type = AST::Nodes::Unary_Operation::Type::reference;
			returns = expr->get_returns();
			returns.ptr++;

			AST::Nodes::Variable* var = dynamic_cast<AST::Nodes::Variable*>(expr.get());
			if (!var) {
				fail("Cannot take the address of something not a variable.");
			}
		} break;
		case Token::Type::star: {
			if (expr->get_returns().ptr <= 0) {
				fail("Cannot dereference a non-pointer type.");
			}

			type = AST::Nodes::Unary_Operation::Type::dereference;
			returns = expr->get_returns().derefs_to();
		} break;
		case Token::Type::bang: {
			if (expr->get_returns().base != Value_Type::Base_Type::boolean) {
				fail("Operand to '!' must be a boolean.");
			}

			type = AST::Nodes::Unary_Operation::Type::bang;
			returns = Value_Type::make_bool();
		} break;
		default:
			fail("Invalid unary operator.");
			break;
	}

	return std::make_unique<AST::Nodes::Unary_Operation>(op, type, returns, std::move(expr));
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_binary_rhs(int prec, std::unique_ptr<AST::Nodes::Expression> lhs) {
	for (;;) {
		if (!is_operator(cur.type)) {
			return lhs;
		}

		i32 token_prec = get_precedence(cur.type);

		if (token_prec < prec) {
			return lhs;
		}

		Token op = cur; /* The operator token. */
		advance();

		auto rhs = parse_unary();

		int next_prec = get_precedence(cur.type);
		if (token_prec < next_prec) {
			rhs = parse_binary_rhs(token_prec + 1, std::move(rhs));
		}

		Value_Type rt = lhs->get_returns();

		AST::Nodes::Binary_Operation::Type type;
		switch (op.type) {
			case Token::Type::plus:
				type = AST::Nodes::Binary_Operation::Type::add;
				break;
			case Token::Type::minus:
				type = AST::Nodes::Binary_Operation::Type::subtract;
				break;
			case Token::Type::slash:
				type = AST::Nodes::Binary_Operation::Type::divide;
				break;
			case Token::Type::star:
				type = AST::Nodes::Binary_Operation::Type::multiply;
				break;
			case Token::Type::percent:
				type = AST::Nodes::Binary_Operation::Type::mod;
				break;
			case Token::Type::assign:
				type = AST::Nodes::Binary_Operation::Type::assign;
				break;
			case Token::Type::equal:
				type = AST::Nodes::Binary_Operation::Type::equal;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::bang_equal:
				type = AST::Nodes::Binary_Operation::Type::not_equal;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::less:
				type = AST::Nodes::Binary_Operation::Type::less;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::less_equal:
				type = AST::Nodes::Binary_Operation::Type::less_equal;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::greater:
				type = AST::Nodes::Binary_Operation::Type::greater;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::greater_equal:
				type = AST::Nodes::Binary_Operation::Type::greater_equal;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::or_:
				if (lhs->get_returns().base != Value_Type::Base_Type::boolean || rhs->get_returns().base != Value_Type::Base_Type::boolean) {
					fail("Operands to '||' must be booleans.");
				}
				type = AST::Nodes::Binary_Operation::Type::or_;
				rt = Value_Type::make_bool();
				break;
			case Token::Type::and_:
				if (lhs->get_returns().base != Value_Type::Base_Type::boolean || rhs->get_returns().base != Value_Type::Base_Type::boolean) {
					fail("Operands to '&&' must be booleans.");
				}
				type = AST::Nodes::Binary_Operation::Type::and_;
				rt = Value_Type::make_bool();
				break;
			default: break;
		}

		lhs = std::make_unique<AST::Nodes::Binary_Operation>(op, type, rt, std::move(lhs), std::move(rhs));
	}
}

std::unique_ptr<AST::Nodes::Number> Parser::parse_number() {
	std::unique_ptr<AST::Nodes::Number> r;
	if (cur.vtype == Value_Type::make_i32()) {
		r = std::make_unique<AST::Nodes::Number>(cur, cur.as_i32());
	} else if (cur.vtype == Value_Type::make_i64()) {
		r = std::make_unique<AST::Nodes::Number>(cur, cur.as_i64());
	} else if (cur.vtype == Value_Type::make_u32()) {
		r = std::make_unique<AST::Nodes::Number>(cur, cur.as_u32());
	} else if (cur.vtype == Value_Type::make_u64()) {
		r = std::make_unique<AST::Nodes::Number>(cur, cur.as_u64());
	} else if (cur.vtype == Value_Type::make_f32()) {
		r = std::make_unique<AST::Nodes::Number>(cur, cur.as_f32());
	} else if (cur.vtype == Value_Type::make_f64()) {
		r = std::make_unique<AST::Nodes::Number>(cur, cur.as_f64());
	} else {
		std::abort();
	}

	advance();
	return std::move(r);
}

std::unique_ptr<AST::Nodes::String> Parser::parse_string() {
	auto r = std::make_unique<AST::Nodes::String>(cur, cur.as_string());

	advance();

	return std::move(r);
}

std::unique_ptr<AST::Nodes::Boolean> Parser::parse_bool(bool v) {
	auto e = std::make_unique<AST::Nodes::Boolean>(cur, v);

	advance();

	return std::move(e);
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_paren() {
	advance();
	auto r = parse_expression();

	if (cur.type != Token::Type::right_paren) {
		fail("Expected ')'.");
	}

	advance();
	
	return r;
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_cast(Value_Type to) {
	Token tok = cur;

	advance();

	auto e = parse_expression();
	const Value_Type& from = e->get_returns();

	if (!Value_Type::castable(e->get_returns(), to) && to.ptr == 0 && from.ptr == 0) {
		fail("Incompatible types.");
	}

	expect(Token::Type::right_paren, "Expected ')'.");

	return std::make_unique<AST::Nodes::Cast>(tok, to, std::move(e));
}

std::unique_ptr<AST::Nodes::Statement> Parser::parse_var_decl(const Value_Type& type) {
	Token tok = cur;

	std::unique_ptr<AST::Nodes::Expression> initialiser;

	if (cur.type != Token::Type::identifier) {
		fail("Expected an identifier");
	}

	std::string name = cur.as_string();

	declare_var(name, type);

	advance();

	if (cur.type == Token::Type::assign) {
		/* Parse the initialiser. */
		advance();
		initialiser = std::move(parse_expression());
	}

	return std::make_unique<AST::Nodes::Variable_Declaration>(tok, name, type, std::move(initialiser));
}

std::unique_ptr<AST::Nodes::Statement> Parser::parse_type_identifier() {
	Value_Type t = Value_Type::make(cur.as_string());

	advance();

	mutate_to_ptr(t);

	/* If the token after the typename is (, treat as a cast.
	 * Otherwise, treat as a variable declaration. */
	if (cur.type != Token::Type::left_paren) {
		return parse_var_decl(t);
	}

	return parse_cast(t);
}

std::shared_ptr<AST::Namespace> Parser::resolve_namespace() {
	auto ns = current_namespace;

	while (cur.type == Token::Type::colon) {
		/* Traverse up the namespace tree from the current namespace
		 * to find a match. */
		auto cns = ns;

		do {
			if (cns->has_sub(prev.as_string())) {
				ns = cns->get_sub(prev.as_string());
				break;
			}

			cns = ns->parent;
		} while (cns != global_namespace);

		advance(); advance();
	}

	return ns;
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_identifier() {
	auto name = cur.as_string();

	Token tok = cur;

	advance();

	Value_Type type = Value_Type::make(name);
	if (Value_Type::valid(name)) {
		mutate_to_ptr(type);
	}

	/* If the token after the identifier is (, treat as a procedure
	 * call or cast. If it is :, treat as a procedure call to a
	 * procedure in a namespace. Otherwise, treat it as a variable. */
	if (cur.type != Token::Type::left_paren && cur.type != Token::Type::colon) {
		if (!var_declared(name)) {
			fail("Undeclared variable.");
		}

		return std::make_unique<AST::Nodes::Variable>(tok, get_var(name), name);
	}

	bool ns_specified = cur.type == Token::Type::colon;

	/* If it's a valid typename, parse it as a cast.
	 * Otherwise, treat is as a procedure call. */
	if (Value_Type::valid(name)) {
		return parse_cast(type);
	}

	auto ns = resolve_namespace();

	if (ns_specified) {
		/* The namespace was specified. */
		name = ns->get_mangled_name() + prev.as_string();
	} else {
		/* Traverse up the namespace tree from the current namespace to
		 * find a matching procedure if no namespace was specified. */
		auto cns = current_namespace;

		while (cns != global_namespace) {
			auto n = cns->get_mangled_name() + name;

			if (prots.count(n) != 0) {
				name = n;
				break;
			}

			cns = cns->parent;
		}
	}

	if (prots.count(name) == 0) {
		fail("Undeclared procedure.");
	}

	tok = prev;

	advance();

	AST::Proc_Prototype* prot = dynamic_cast<AST::Proc_Prototype*>(prots[name].get());
	if (!prot) {
		fail("Undeclared procedure.");
	}

	std::vector<std::unique_ptr<AST::Nodes::Expression>> args;
	if (cur.type != Token::Type::right_paren) {
		usize i;
		for (i = 0;; i++) {
			if (i >= prot->get_args().size() && prot->get_args().size() != 0 && i != 0) {
				fail("Incorrect number of arguments passed to procedure.");
			}

			auto e = parse_expression();

			if (e->get_returns() != prot->get_args()[i].type) {
				fail("Type mismatch.");
			}

			args.push_back(std::move(e));

			if (cur.type == Token::Type::right_paren) {
				break;
			}

			if (cur.type != Token::Type::comma) {
				fail_at_cur("Expected ')'.");
			}

			advance();
		}

		if (i + 1 != prot->get_args().size()) {
			fail("Incorrect number of arguments passed to procedure.");
		}
	}

	advance();

	return std::make_unique<AST::Nodes::Call>(tok, name, prot->get_value_type(), std::move(args), ns);
}

std::unique_ptr<AST::Nodes::Return> Parser::parse_return() {
	Token tok = cur;

	advance();

	if (cur.type == Token::Type::semicolon) {
		/* Returning void. */

		if (Value_Type::make_void() != current_proc_returns) {
			fail("Type mismatch.");
		}

		return std::make_unique<AST::Nodes::Return>(tok, null);
	}

	auto e = parse_expression();

	if (e->get_returns() != current_proc_returns) {
		fail("Type mismatch.");
	}

	return std::make_unique<AST::Nodes::Return>(tok, std::move(e));
}

std::unique_ptr<AST::Nodes::If> Parser::parse_if() {
	Token tok = cur;

	advance();

	auto cond = parse_expression();

	if (cond->get_returns().base != Value_Type::Base_Type::boolean) {
		fail("Condition of 'if' must evaluate to a boolean.");
	}

	auto then = parse_block();

	if (cur.type == Token::Type::else_) {
		/* Parse the else clause. */
		advance();

		auto else_ = parse_block();

		return std::make_unique<AST::Nodes::If>(tok, std::move(cond), std::move(then), std::move(else_));
	} else if (cur.type == Token::Type::elif) {
		/* Parse else-ifs. */
		auto else_ = parse_if();

		return std::make_unique<AST::Nodes::If>(tok, std::move(cond), std::move(then), std::move(else_));
	}

	return std::make_unique<AST::Nodes::If>(tok, std::move(cond), std::move(then));
}

std::unique_ptr<AST::Nodes::Loop> Parser::parse_loop() {
	advance();

	return std::make_unique<AST::Nodes::Loop>(prev, parse_block());
}

std::unique_ptr<AST::Nodes::While> Parser::parse_while() {
	Token tok = cur;

	advance();

	auto cond = parse_expression();

	if (cond->get_returns().base != Value_Type::Base_Type::boolean) {
		fail("Condition of 'while' must evaluate to a boolean.");
	}

	return std::make_unique<AST::Nodes::While>(tok, std::move(cond), parse_block());
}

std::unique_ptr<AST::Nodes::For> Parser::parse_for() {
	Token tok = cur;

	advance();

	begin_scope();

	bool req_semi;

	auto decl = parse_statement(req_semi);
	expect(Token::Type::semicolon, "Expected ';' after statement.");
	auto cond = parse_expression();
	expect(Token::Type::semicolon, "Expected ';' after expression.");
	auto incr = parse_expression();

	auto body = parse_block();

	end_scope();

	return std::make_unique<AST::Nodes::For>(tok, std::move(decl), std::move(cond), std::move(incr), std::move(body));
}

std::unique_ptr<AST::Nodes::Break> Parser::parse_break() {
	advance();

	return std::make_unique<AST::Nodes::Break>(prev);
}

std::unique_ptr<AST::Nodes::Continue> Parser::parse_continue() {
	advance();

	return std::make_unique<AST::Nodes::Continue>(prev);
}

std::unique_ptr<AST::Nodes::Statement> Parser::parse_statement(bool& req_semi) {
	req_semi = true;

	switch (cur.type) {
		case Token::Type::return_:    return parse_return();
		case Token::Type::semicolon:
			/* Ignore floating semicolons. */
			advance();
			return parse_statement(req_semi);
		case Token::Type::if_:        req_semi = false; return parse_if();
		case Token::Type::loop:       req_semi = false; return parse_loop();
		case Token::Type::while_:     req_semi = false; return parse_while();
		case Token::Type::for_:       req_semi = false; return parse_for();
		case Token::Type::break_:     return parse_break();
		case Token::Type::continue_:  return parse_continue();
		case Token::Type::left_brace: req_semi = false; return parse_block();
		case Token::Type::identifier: {
			auto s = cur.as_string();
			if (Value_Type::valid(s)) {
				return parse_type_identifier();
			}

			return parse_expression();
		}
		default: break;
	};

	fail_at_cur("Invalid statement.");
	return null;
}

std::unique_ptr<AST::Nodes::Expression> Parser::parse_primary() {
	switch (cur.type) {
		case Token::Type::identifier:
				return parse_identifier();
		case Token::Type::number:     return parse_number();
		case Token::Type::string:     return parse_string();
		case Token::Type::true_:      return parse_bool(true);
		case Token::Type::false_:     return parse_bool(false);
		case Token::Type::left_paren: return parse_paren();
		default:
			fail("Unexpected token.");
			return null;
	};
}

std::unique_ptr<AST::Nodes::Block> Parser::parse_block() {
	std::vector<std::unique_ptr<AST::Nodes::Statement>> stmts;

	bool has_return = false;

	Token tok = cur;

	expect(Token::Type::left_brace, "Expected '{'.");

	begin_scope();

	while (cur.type != Token::Type::right_brace) {
		bool req_semi;
		auto e = parse_statement(req_semi);

		/* Not all statements are followed by a semicolon - If statements and
		 * blocks, for example. */
		if (req_semi) {
			expect(Token::Type::semicolon, "Expected ';'");
		}

		auto sub = dynamic_cast<AST::Nodes::Block*>(e.get());
		if (sub && sub->get_has_return()) {
			has_return = true;
		}

		auto ret = dynamic_cast<AST::Nodes::Return*>(e.get());
		if (ret) {
			has_return = true;
		}

		auto br = dynamic_cast<AST::Nodes::Break*>(e.get());
		if (br) {
			has_return = true;
		}

		auto cont = dynamic_cast<AST::Nodes::Continue*>(e.get());
		if (cont) {
			has_return = true;
		}

		stmts.push_back(std::move(e));
	}

	expect(Token::Type::right_brace, "Expected '}'.");

	end_scope();

	return std::make_unique<AST::Nodes::Block>(tok, std::move(stmts), has_return);
}

void Parser::mutate_to_ptr(Value_Type& t) {
	/* If the token after the identifier is *, treat as a pointer. */
	if (cur.type == Token::Type::star) {
		advance();
		t.ptr++;

		while (cur.type == Token::Type::star) {
			t.ptr++;
			advance();
		}
	}
}

std::shared_ptr<AST::Declaration> Parser::parse_proc_decl(bool is_extern) {
	Value_Type type = Value_Type::make(cur.as_string());

	advance();

	mutate_to_ptr(type);

	current_proc_returns = type;

	if (cur.type != Token::Type::identifier) {
		fail("Expected an identifier");
	}

	Token tok = cur;

	std::string name = current_namespace != global_namespace ? current_namespace->get_mangled_name() + cur.as_string() : cur.as_string();

	advance();

	expect(Token::Type::left_paren, "Expected '('");

	std::vector<AST::Argument> args;

	while (cur.type == Token::Type::identifier) {
		if (cur.type != Token::Type::identifier) {
			fail("Expected a type name.");
		}

		std::string type_name = cur.as_string();
		if (!Value_Type::valid(type_name)) {
			fail("Invalid typename.");
		}

		Value_Type t = Value_Type::make(type_name);

		advance();

		mutate_to_ptr(t);

		if (cur.type != Token::Type::identifier) {
			fail("Expected an argument name.");
		}

		AST::Argument arg;
		arg.name = cur.as_string();
		arg.type = t;
		arg.token = cur;

		args.push_back(std::move(arg));
		advance();

		if (cur.type != Token::Type::right_paren) {
			expect(Token::Type::comma, "Expected ','");
		}
	}

	expect(Token::Type::right_paren, "Expected ')'.");

	auto prot = std::make_shared<AST::Proc_Prototype>(name, type, args, is_extern);
	prots[name] = prot;

	if (cur.type == Token::Type::semicolon) {
		advance();
		return prot;
	}

	begin_scope();
	for (auto& arg : args) {
		declare_var(arg.name, arg.type);
	}

	auto block = parse_block();
	end_scope();

	return std::make_shared<AST::Procedure>(tok,
		std::make_unique<AST::Proc_Prototype>(name, type, args, is_extern),
		std::move(block));

	return null;
}

std::shared_ptr<AST::Declaration> Parser::parse_extern_decl() {
	advance();
	return parse_proc_decl(true);
}

std::shared_ptr<AST::Namespace> Parser::parse_namespace() {
	auto old_namespace = current_namespace;

	advance();

	if (cur.type != Token::Type::identifier) {
		fail_at_cur("Expected an identifier.");
	}

	auto new_namespace = std::make_shared<AST::Namespace>(cur.as_string(), current_namespace);
	current_namespace = new_namespace;

	advance();

	expect(Token::Type::left_brace, "Expected '{'.");

	while (cur.type != Token::Type::right_brace) {
		parse_root();
	}

	expect(Token::Type::right_brace, "Expected '}'.");

	current_namespace = old_namespace;

	return new_namespace;
}

void Parser::parse_root() {
	switch (cur.type) {
		case Token::Type::end:
			break;
		case Token::Type::identifier: {
			if (!Value_Type::valid(cur.as_string())) {
				fail("Invalid typename.");
			}

			auto decl = parse_proc_decl();
			if (decl->get_type() == AST::Declaration::Type::procedure) {
				current_namespace->add_proc(std::static_pointer_cast<AST::Procedure>(decl));
			} else if (decl->get_type() == AST::Declaration::Type::prototype) {
				current_namespace->add_prot(std::static_pointer_cast<AST::Proc_Prototype>(decl));
			} else {
				fail("Invalid declaration.");
			}
		} break;
		case Token::Type::extern_:
			current_namespace->add_prot(std::static_pointer_cast<AST::Proc_Prototype>(parse_extern_decl()));
			break;
		case Token::Type::namespace_: {
			auto n = parse_namespace();
			current_namespace->add_sub(n);
		} break;
		default:
			break;
	}
}

std::shared_ptr<ParseResult> Parser::parse() {
	global_namespace = std::make_shared<AST::Namespace>("<global>");
	current_namespace = global_namespace;

	advance();

	while (cur.type != Token::Type::end) {
		parse_root();
	}

	return std::make_shared<ParseResult>(lexer.get_module_name(), lexer.source, global_namespace);
}
