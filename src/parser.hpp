#pragma once

#include <memory>
#include <unordered_map>
#include <vector>

#include "ast.hpp"
#include "common.h"
#include "lexer.hpp"

class ParseResult {
private:
	std::shared_ptr<AST::Namespace> ns;

	std::string module_name;

	const char* source;

	friend class Parser;
	friend class Generator_LLVM;
public:
	ParseResult(const std::string& module_name, const char* source,
		const std::shared_ptr<AST::Namespace>& ns)
		: source(source), module_name(module_name), ns(ns) {}

	const std::string& get_module_name() const {
		return module_name;
	}
};

class Parser {
private:
	Lexer& lexer;

	Token prev;
	Token cur;

	/* A stack of maps for variables is used to type-check variables,
	 * where a new map can be pushed onto the stack for each new
	 * scope, and subsequently popped off at the end of the scope.
	 * This stack is used purely for type checking.
	 *
	 * Getting variables traverses the entire stack, while declaring
	 * them adds to map at the top of the stack.
	 *
	 * std::vector is required instead of std::stack because the
	 * stack must be iterated. */
	std::vector<std::unordered_map<std::string, Value_Type>> vars;
	std::unordered_map<std::string, std::shared_ptr<AST::Declaration>> prots;

	void mutate_to_ptr(Value_Type& t);

	std::unique_ptr<AST::Nodes::Statement> parse_statement(bool& req_semi);
	std::unique_ptr<AST::Nodes::Expression> parse_expression();
	std::unique_ptr<AST::Nodes::Expression> parse_primary();
	std::unique_ptr<AST::Nodes::Expression> parse_binary_rhs(int prec, std::unique_ptr<AST::Nodes::Expression> lhs);
	std::unique_ptr<AST::Nodes::Number> parse_number();
	std::unique_ptr<AST::Nodes::String> parse_string();
	std::unique_ptr<AST::Nodes::Boolean> parse_bool(bool v);
	std::unique_ptr<AST::Nodes::Expression> parse_unary();
	std::unique_ptr<AST::Nodes::Expression> parse_paren();
	std::unique_ptr<AST::Nodes::Expression> parse_identifier();
	std::unique_ptr<AST::Nodes::Statement> parse_type_identifier();
	std::unique_ptr<AST::Nodes::Return> parse_return();
	std::unique_ptr<AST::Nodes::If> parse_if();
	std::unique_ptr<AST::Nodes::Loop> parse_loop();
	std::unique_ptr<AST::Nodes::While> parse_while();
	std::unique_ptr<AST::Nodes::For> parse_for();
	std::unique_ptr<AST::Nodes::Break> parse_break();
	std::unique_ptr<AST::Nodes::Continue> parse_continue();
	std::unique_ptr<AST::Nodes::Expression> parse_cast(Value_Type to);
	std::unique_ptr<AST::Nodes::Block> parse_block();
	std::unique_ptr<AST::Nodes::Statement> parse_var_decl(const Value_Type& type);
	std::shared_ptr<AST::Namespace> parse_namespace();
	std::shared_ptr<AST::Namespace> resolve_namespace();
	std::shared_ptr<AST::Declaration> parse_decl(bool is_extern = false);
	std::shared_ptr<AST::Declaration> parse_proc_decl(bool is_extern = false);
	std::shared_ptr<AST::Declaration> parse_extern_decl();

	void parse_root();

	Value_Type current_proc_returns;

	bool var_declared(const std::string& name);
	void declare_var(const std::string& name, Value_Type type);
	Value_Type get_var(const std::string& name);
	void begin_scope();
	void end_scope();

	void fail_at(const char* message, const Token& token);
	void fail_at_cur(const char* message);
	void fail_at_prev(const char* message);
	void fail(const char* message);

	void advance();
	void expect(Token::Type type, const char* err);

	std::shared_ptr<AST::Namespace> current_namespace;

	std::shared_ptr<AST::Namespace> global_namespace;
public:
	Parser(Lexer& lexer);

	std::shared_ptr<ParseResult> parse();

	Lexer& get_lexer() {
		return lexer;
	}
};
