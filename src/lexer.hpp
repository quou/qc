#pragma once

#include "common.h"
#include "type.hpp"

class Lexer;

struct Token {
	enum class Type {
		left_paren,   right_paren,
		left_brace,   right_brace,
		left_bracket, right_bracket,
		semicolon,
		identifier,
		proc,
		extern_,
		string,
		number,
		comma,
		plus, minus,
		star, slash,
		percent,
		ampersand,

		namespace_,
		colon,

		if_, else_, elif,
		loop, break_, continue_,
		while_, for_,

		bang,
		and_,
		or_,

		pipe,

		true_, false_,

		greater, greater_equal, less, less_equal,
		equal, bang_equal,

		assign,

		return_,

		error, end
	} type;

	Value_Type vtype;

	const char* start;
	usize len;
	u32 line;

	static Token error(const Lexer& lexer, const char* message);
	static Token make(const Lexer& lexer, Type type);

	i32 as_i32();
	i64 as_i64();
	u32 as_u32();
	u64 as_u64();
	f32 as_f32();
	f64 as_f64();
	std::string as_string();
};

class Lexer {
private:
	const char* cur;
	const char* start;
	const char* source;

	std::string module_name;

	u32 line;

	inline char advance()   { return *cur++; }
	inline char peek()      { return *cur; }
	inline char peek_next() { return cur[1]; }

	bool match(char e);

	Token::Type get_identifier_type();

	Token lex_string();
	Token lex_number();
	Token lex_identifier();

	void skip_whitespace();

	friend class Token;
	friend class Parser;
public:
	/* source must be kept alive throughout compilation. */
	Lexer(const std::string& module_name, const char* source);

	Token next();

	inline bool at_end() const { return *cur == '\0'; }

	class Iterator {
	private:
		Token cur_tok;
		Lexer& l;

		friend class Lexer;
	public:
		Iterator(Lexer& l, const Token& t);

		Iterator& operator++();
		bool operator!=(const Iterator& other) const;
		Token operator*();
	};

	Iterator begin();
	Iterator end();

	const std::string& get_module_name() {
		return module_name;
	}
};
