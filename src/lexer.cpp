#include <cstring>

#include <string>
#include <unordered_map>

#include "lexer.hpp"

std::unordered_map<std::string, Token::Type> keywords = {
	{ "proc",     Token::Type::proc },
	{ "extern",   Token::Type::extern_ },
	{ "return",   Token::Type::return_ },
	{ "true",     Token::Type::true_ },
	{ "false",    Token::Type::false_ },
	{ "if",       Token::Type::if_ },
	{ "else",     Token::Type::else_ },
	{ "elif",     Token::Type::elif },
	{ "loop",     Token::Type::loop },
	{ "break",    Token::Type::break_ },
	{ "continue", Token::Type::continue_ },
	{ "while",    Token::Type::while_ },
	{ "for",      Token::Type::for_ },
	{ "namespace",Token::Type::namespace_ }
};

static bool is_digit(char c) {
	return c >= '0' && c <= '9';
}

static bool is_alpha(char c) {
	return
		(c >= 'a' && c <= 'z') ||
		(c >= 'A' && c <= 'Z');
}

Token Token::error(const Lexer& lexer, const char* message) {
	return Token {
		.type = Type::error,
		.start = message,
		.len = std::strlen(message),
		.line = lexer.line
	};
}

Token Token::make(const Lexer& lexer, Type ntype) {
	return Token {
		.type = ntype,
		.start = lexer.start,
		.len = (usize)(lexer.cur - lexer.start),
		.line = lexer.line
	};
}

i32 Token::as_i32() {
	return std::stoi(as_string());
}

i64 Token::as_i64() {
	return std::stoll(as_string());
}

u32 Token::as_u32() {
	return (u32)std::stoll(as_string());
}

u64 Token::as_u64() {
	return (u64)std::stoll(as_string());
}

f32 Token::as_f32() {
	return std::stof(as_string());
}

f64 Token::as_f64() {
	return std::stod(as_string());
}

std::string Token::as_string() {
	return std::string(start, len);
}

void Lexer::skip_whitespace() {
	/* TODO: Skip comments */
	for (;;) {
		char c = peek();

		switch (c) {
			case ' ':
			case '\r':
			case '\t':
				advance();
				break;
			case '\n':
				line++;
				advance();
				break;
			default: return;
		}
	}
}

#include <iostream>

Token Lexer::lex_string() {
	start++;
	while (peek() != '"' && !at_end()) {
		if (peek() == '\n') { line++; }
		advance();
	}

	if (at_end()) {
		return Token::error(*this, "Unterminated string.");
	}

	auto r = Token::make(*this, Token::Type::string);

	advance();

	return r;
}

Token::Type Lexer::get_identifier_type() {
	std::string id_string(start, (usize)(cur - start));

	for (const auto& mapping : keywords) {
		if (mapping.first == id_string) {
			return mapping.second;
		}
	}

	return Token::Type::identifier;
}

Token Lexer::lex_number() {
	while (is_digit(peek())) { advance(); }

	bool is_float = false;

	if (peek() == '.' && is_digit(peek_next())) {
		advance();

		while (is_digit(peek())) { advance(); }

		is_float = true;
	}

	Token t = Token::make(*this, Token::Type::number);

	/* TODO: Handle suffixes. */
	if (is_float) {
		t.vtype = Value_Type::make_f64();
	} else {
		t.vtype = Value_Type::make_i64();
	}

	return t;
}

Token Lexer::lex_identifier() {
	while (is_alpha(peek()) || is_digit(peek()) || peek() == '_') {
		advance();
	}

	return Token::make(*this, get_identifier_type());
}

Lexer::Lexer(const std::string& module_name, const char* source)
	: module_name(module_name), cur(source), start(source), source(source), line(1) {}

#include <iostream>

bool Lexer::match(char e) {
	if (at_end()) { return false; }
	if (*cur != e) { return false; }
	cur++;
	return true;
}

Token Lexer::next() {
	skip_whitespace();

	start = cur;

	char c = advance();

	if (c == '\0') {
		return Token::make(*this, Token::Type::end);
	}

	if (is_digit(c)) { return lex_number(); }
	if (is_alpha(c)) { return lex_identifier(); }

	switch (c) {
		case '(': return Token::make(*this, Token::Type::left_paren);
		case ')': return Token::make(*this, Token::Type::right_paren);
		case '{': return Token::make(*this, Token::Type::left_brace);
		case '}': return Token::make(*this, Token::Type::right_brace);
		case '[': return Token::make(*this, Token::Type::left_bracket);
		case ']': return Token::make(*this, Token::Type::right_bracket);
		case ':': return Token::make(*this, Token::Type::colon);
		case ';': return Token::make(*this, Token::Type::semicolon);
		case ',': return Token::make(*this, Token::Type::comma);
		case '+': return Token::make(*this, Token::Type::plus);
		case '-': return Token::make(*this, Token::Type::minus);
		case '*': return Token::make(*this, Token::Type::star);
		case '/': return Token::make(*this, Token::Type::slash);
		case '=': return Token::make(*this, match('=') ? Token::Type::equal         : Token::Type::assign);
		case '>': return Token::make(*this, match('=') ? Token::Type::greater_equal : Token::Type::greater);
		case '<': return Token::make(*this, match('=') ? Token::Type::less_equal    : Token::Type::less);
		case '&': return Token::make(*this, match('&') ? Token::Type::and_          : Token::Type::ampersand);
		case '!': return Token::make(*this, match('=') ? Token::Type::bang_equal    : Token::Type::bang);
		case '|': return Token::make(*this, match('|') ? Token::Type::or_           : Token::Type::pipe);
			return Token::make(*this, Token::Type::pipe);
		case '%': return Token::make(*this, Token::Type::percent);
		case '"': return lex_string();
	}

	return Token::error(*this, "Unexpected character.");
}

Lexer::Iterator::Iterator(Lexer& l, const Token& t) : l(l), cur_tok(t) {}

Lexer::Iterator& Lexer::Iterator::operator++() {
	cur_tok = l.next();
	return *this;
}

bool Lexer::Iterator::operator!=(const Iterator& other) const {
	return cur_tok.type != other.cur_tok.type || &l != &other.l;
}

Token Lexer::Iterator::operator*() {
	return cur_tok;
}

Lexer::Iterator Lexer::begin() {
		start = source;
		cur = source;
		line = 1;
		return Iterator(*this, next());
}

Lexer::Iterator Lexer::end() {
	return Iterator(*this, Token::make(*this, Token::Type::end));
}
