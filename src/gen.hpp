#pragma once

#include <memory>
#include <string>
#include <vector>
#include <stack>

#include "common.h"
#include "parser.hpp"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/IRBuilder.h>

namespace llvm {
	class AllocaInst;
	class BasicBlock;
}

class Generator_LLVM {
private:
	std::shared_ptr<ParseResult> parse_result;

	/* The generator mirrors the parser's variable stack.
	 * This one is for keeping track of actual data while the code
	 * is being generated. Types are checked in the parser. */
	std::vector<std::unordered_map<std::string, llvm::AllocaInst*>> vars;

	std::stack<llvm::BasicBlock*> loopconts;
	std::stack<llvm::BasicBlock*> loopstarts;

	bool var_declared(const std::string& name);
	void declare_var(const Token& tok, const std::string& name, llvm::AllocaInst* dat);
	llvm::AllocaInst* get_var(const Token& tok, const std::string& name);
	void begin_scope();
	void end_scope();

	llvm::Function* current_fun;

	llvm::LLVMContext ctx;
	std::unique_ptr<llvm::IRBuilder<>> builder;
	std::unique_ptr<llvm::Module> module;

	llvm::Type* gen_type(Value_Type type);

	friend class AST::Nodes::Binary_Operation;
	friend class AST::Nodes::Block;
	friend class AST::Nodes::Boolean;
	friend class AST::Nodes::Break;
	friend class AST::Nodes::Call;
	friend class AST::Nodes::Cast;
	friend class AST::Nodes::Continue;
	friend class AST::Nodes::For;
	friend class AST::Nodes::If;
	friend class AST::Nodes::Loop;
	friend class AST::Nodes::Number;
	friend class AST::Nodes::Return;
	friend class AST::Nodes::String;
	friend class AST::Nodes::Unary_Operation;
	friend class AST::Nodes::Variable;
	friend class AST::Nodes::Variable_Declaration;
	friend class AST::Nodes::While;
	friend class AST::Proc_Prototype;
	friend class AST::Procedure;

	llvm::AllocaInst* create_entry_alloca(llvm::Function* fun, const Value_Type& type, const std::string& name);
public:
	Generator_LLVM(const std::shared_ptr<ParseResult>& parse_result);
	
	void generate(const std::string& outfilename = "a.o");
	void fail(const AST::Nodes::Statement* stmt, const char* message);
	void fail(const Token& tok, const char* message);
};
