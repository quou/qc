#include <stdio.h>

#include <check.h>

#include "common.h"

#include "suites/add.h"
#include "suites/sub.h"

/* TODO: Tests for casts, division and multiplication. */

i32 run_suite(Suite* s) {
	SRunner* sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	i32 failed_count = srunner_ntests_failed(sr);
	srunner_free(sr);

	return failed_count;
}

i32 main() {
	i32 failed_count = 0;

	failed_count += run_suite(add_suite());
	failed_count += run_suite(sub_suite());

	return (failed_count == 0) ? 0 : 1;
}
