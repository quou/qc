#include <check.h>

#include "common.h"

extern i64 add_i64(i64 a, i64 b);
extern i32 add_i32(i32 a, i32 b);
extern u64 add_u64(u64 a, u64 b);
extern u32 add_u32(u32 a, u32 b);
extern f64 add_f64(f64 a, f64 b);
extern f32 add_f32(f32 a, f32 b);

START_TEST(test_add_i64_a) {
	ck_assert(add_i64(1, 1) == 2);
} END_TEST

START_TEST(test_add_i64_b) {
	ck_assert(add_i64(39, 15) == 54);
} END_TEST

START_TEST(test_add_i64_c) {
	ck_assert(add_i64(-3, 6) == 3);
} END_TEST

START_TEST(test_add_i64_d) {
	ck_assert(add_i64(1050, 16) == 1066);
} END_TEST

START_TEST(test_add_i32_a) {
	ck_assert(add_i32(1, 1) == 2);
} END_TEST

START_TEST(test_add_i32_b) {
	ck_assert(add_i32(39, 15) == 54);
} END_TEST

START_TEST(test_add_i32_c) {
	ck_assert(add_i32(-3, 6) == 3);
} END_TEST

START_TEST(test_add_i32_d) {
	ck_assert(add_i32(1050, 16) == 1066);
} END_TEST

START_TEST(test_add_u64_a) {
	ck_assert(add_u64(1, 1) == 2);
} END_TEST

START_TEST(test_add_u64_b) {
	ck_assert(add_u64(39, 15) == 54);
} END_TEST

START_TEST(test_add_u64_c) {
	ck_assert(add_u64(3, 6) == 9);
} END_TEST

START_TEST(test_add_u64_d) {
	ck_assert(add_u64(1050, 16) == 1066);
} END_TEST

START_TEST(test_add_u32_a) {
	ck_assert(add_u32(1, 1) == 2);
} END_TEST

START_TEST(test_add_u32_b) {
	ck_assert(add_u32(39, 15) == 54);
} END_TEST

START_TEST(test_add_u32_c) {
	ck_assert(add_u32(3, 6) == 9);
} END_TEST

START_TEST(test_add_u32_d) {
	ck_assert(add_u32(1050, 16) == 1066);
} END_TEST

START_TEST(test_add_f64_a) {
	ck_assert_float_eq(add_f64(1.0, 1.0), 2.0);
} END_TEST

START_TEST(test_add_f64_b) {
	ck_assert_float_eq(add_f64(1.5, 1.5), 3.0);
} END_TEST

START_TEST(test_add_f64_c) {
	ck_assert_float_eq(add_f64(-3.0, 6.56), 3.56);
} END_TEST

START_TEST(test_add_f64_d) {
	ck_assert_float_eq(add_f64(1050.5, 16), 1066.5);
} END_TEST

START_TEST(test_add_f32_a) {
	ck_assert_float_eq(add_f32(1.0, 1.0), 2.0);
} END_TEST

START_TEST(test_add_f32_b) {
	ck_assert_float_eq(add_f32(1.5, 1.5), 3.0);
} END_TEST

START_TEST(test_add_f32_c) {
	ck_assert_float_eq(add_f32(-3.0, 6.56), 3.56);
} END_TEST

START_TEST(test_add_f32_d) {
	ck_assert_float_eq(add_f32(1050.5, 16), 1066.5);
} END_TEST

Suite* add_suite(void) {
	Suite* s = suite_create("Addition");

	TCase* tc_add_i64 = tcase_create("add_i64");
	tcase_add_test(tc_add_i64, test_add_i64_a);
	tcase_add_test(tc_add_i64, test_add_i64_b);
	tcase_add_test(tc_add_i64, test_add_i64_c);
	tcase_add_test(tc_add_i64, test_add_i64_d);
	suite_add_tcase(s, tc_add_i64);

	TCase* tc_add_i32 = tcase_create("add_i32");
	tcase_add_test(tc_add_i32, test_add_i32_a);
	tcase_add_test(tc_add_i32, test_add_i32_b);
	tcase_add_test(tc_add_i32, test_add_i32_c);
	tcase_add_test(tc_add_i32, test_add_i32_d);
	suite_add_tcase(s, tc_add_i32);

	TCase* tc_add_u64 = tcase_create("add_u64");
	tcase_add_test(tc_add_u64, test_add_u64_a);
	tcase_add_test(tc_add_u64, test_add_u64_b);
	tcase_add_test(tc_add_u64, test_add_u64_c);
	tcase_add_test(tc_add_u64, test_add_u64_d);
	suite_add_tcase(s, tc_add_u64);

	TCase* tc_add_u32 = tcase_create("add_u32");
	tcase_add_test(tc_add_u32, test_add_u32_a);
	tcase_add_test(tc_add_u32, test_add_u32_b);
	tcase_add_test(tc_add_u32, test_add_u32_c);
	tcase_add_test(tc_add_u32, test_add_u32_d);
	suite_add_tcase(s, tc_add_u32);

	TCase* tc_add_f64 = tcase_create("add_f64");
	tcase_add_test(tc_add_f64, test_add_f64_a);
	tcase_add_test(tc_add_f64, test_add_f64_b);
	tcase_add_test(tc_add_f64, test_add_f64_c);
	tcase_add_test(tc_add_f64, test_add_f64_d);
	suite_add_tcase(s, tc_add_f64);

	TCase* tc_add_f32 = tcase_create("add_f32");
	tcase_add_test(tc_add_f32, test_add_f32_a);
	tcase_add_test(tc_add_f32, test_add_f32_b);
	tcase_add_test(tc_add_f32, test_add_f32_c);
	tcase_add_test(tc_add_f32, test_add_f32_d);
	suite_add_tcase(s, tc_add_f32);

	return s;
}
