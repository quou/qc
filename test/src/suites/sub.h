#include <check.h>

#include "common.h"

extern i64 sub_i64(i64 a, i64 b);
extern i32 sub_i32(i32 a, i32 b);
extern u64 sub_u64(u64 a, u64 b);
extern u32 sub_u32(u32 a, u32 b);
extern f64 sub_f64(f64 a, f64 b);
extern f32 sub_f32(f32 a, f32 b);

START_TEST(test_sub_i64_a) {
	ck_assert(sub_i64(59, 4) == 55);
} END_TEST

START_TEST(test_sub_i64_b) {
	ck_assert(sub_i64(39, 15) == 24);
} END_TEST

START_TEST(test_sub_i64_c) {
	ck_assert(sub_i64(-3, 6) == -9);
} END_TEST

START_TEST(test_sub_i64_d) {
	ck_assert(sub_i64(1050, 16) == 1034);
} END_TEST

START_TEST(test_sub_i32_a) {
	ck_assert(sub_i32(59, 4) == 55);
} END_TEST

START_TEST(test_sub_i32_b) {
	ck_assert(sub_i32(39, 15) == 24);
} END_TEST

START_TEST(test_sub_i32_c) {
	ck_assert(sub_i32(-3, 6) == -9);
} END_TEST

START_TEST(test_sub_i32_d) {
	ck_assert(sub_i32(1050, 16) == 1034);
} END_TEST

START_TEST(test_sub_u64_a) {
	ck_assert(sub_u64(59, 4) == 55);
} END_TEST

START_TEST(test_sub_u64_b) {
	ck_assert(sub_u64(39, 15) == 24);
} END_TEST

START_TEST(test_sub_u64_c) {
	ck_assert(sub_u64(6, 3) == 3);
} END_TEST

START_TEST(test_sub_u64_d) {
	ck_assert(sub_u64(1050, 16) == 1034);
} END_TEST

START_TEST(test_sub_u32_a) {
	ck_assert(sub_u32(59, 4) == 55);
} END_TEST

START_TEST(test_sub_u32_b) {
	ck_assert(sub_u32(39, 15) == 24);
} END_TEST

START_TEST(test_sub_u32_c) {
	ck_assert(sub_u32(6, 3) == 3);
} END_TEST

START_TEST(test_sub_u32_d) {
	ck_assert(sub_u32(1050, 16) == 1034);
} END_TEST

START_TEST(test_sub_f64_a) {
	ck_assert_double_eq(sub_f64(59.5, 4.5), 55.0);
} END_TEST

START_TEST(test_sub_f64_b) {
	ck_assert_double_eq(sub_f64(39.0, 15.0), 24.0);
} END_TEST

START_TEST(test_sub_f64_c) {
	ck_assert_double_eq(sub_f64(-3.0, 6.4), -9.4);
} END_TEST

START_TEST(test_sub_f64_d) {
	ck_assert_double_eq(sub_f64(1050.0, 16.165), 1033.835);
} END_TEST

START_TEST(test_sub_f32_a) {
	ck_assert_float_eq(sub_f32(59.5f, 4.5f), 55.0f);
} END_TEST

START_TEST(test_sub_f32_b) {
	ck_assert_float_eq(sub_f32(39.0f, 15.0f), 24.0f);
} END_TEST

START_TEST(test_sub_f32_c) {
	ck_assert_float_eq(sub_f32(-3.0f, 6.4f), -9.4f);
} END_TEST

START_TEST(test_sub_f32_d) {
	ck_assert_float_eq(sub_f32(1050.0f, 16.165f), 1033.835f);
} END_TEST

Suite* sub_suite(void) {
	Suite* s = suite_create("Subtraction");

	TCase* tc_sub_i64 = tcase_create("sub_i64");
	tcase_add_test(tc_sub_i64, test_sub_i64_a);
	tcase_add_test(tc_sub_i64, test_sub_i64_b);
	tcase_add_test(tc_sub_i64, test_sub_i64_c);
	tcase_add_test(tc_sub_i64, test_sub_i64_d);
	suite_add_tcase(s, tc_sub_i64);

	TCase* tc_sub_i32 = tcase_create("sub_i32");
	tcase_add_test(tc_sub_i32, test_sub_i32_a);
	tcase_add_test(tc_sub_i32, test_sub_i32_b);
	tcase_add_test(tc_sub_i32, test_sub_i32_c);
	tcase_add_test(tc_sub_i32, test_sub_i32_d);
	suite_add_tcase(s, tc_sub_i32);

	TCase* tc_sub_u64 = tcase_create("sub_u64");
	tcase_add_test(tc_sub_u64, test_sub_u64_a);
	tcase_add_test(tc_sub_u64, test_sub_u64_b);
	tcase_add_test(tc_sub_u64, test_sub_u64_c);
	tcase_add_test(tc_sub_u64, test_sub_u64_d);
	suite_add_tcase(s, tc_sub_u64);

	TCase* tc_sub_u32 = tcase_create("sub_u32");
	tcase_add_test(tc_sub_u32, test_sub_u32_a);
	tcase_add_test(tc_sub_u32, test_sub_u32_b);
	tcase_add_test(tc_sub_u32, test_sub_u32_c);
	tcase_add_test(tc_sub_u32, test_sub_u32_d);
	suite_add_tcase(s, tc_sub_u32);

	TCase* tc_sub_f64 = tcase_create("sub_f64");
	tcase_add_test(tc_sub_f64, test_sub_f64_a);
	tcase_add_test(tc_sub_f64, test_sub_f64_b);
	tcase_add_test(tc_sub_f64, test_sub_f64_c);
	tcase_add_test(tc_sub_f64, test_sub_f64_d);
	suite_add_tcase(s, tc_sub_f64);

	TCase* tc_sub_f32 = tcase_create("sub_f32");
	tcase_add_test(tc_sub_f32, test_sub_f32_a);
	tcase_add_test(tc_sub_f32, test_sub_f32_b);
	tcase_add_test(tc_sub_f32, test_sub_f32_c);
	tcase_add_test(tc_sub_f32, test_sub_f32_d);
	suite_add_tcase(s, tc_sub_f32);

	return s;
}
