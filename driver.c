#include <stdio.h>

#include <stdbool.h>

extern long do_a_thing(long, long, long);
extern long f();
extern long* get_a_ptr();
extern long deref_a_ptr(long* p);
extern long* offset_ptr(long* p);

extern unsigned int add_uints(unsigned int a, unsigned int b);

extern unsigned long up_cast(int in);
extern float trunc_f64(double in);
extern int* cast_ptr(long* in);
extern bool get_a_bool();
extern long are_ints_equal(long a, long b);
extern bool get_not(bool a);
extern long print_hello();
extern void fizz_buzz(long n);
extern bool compare(long a, long b);
extern long get_a_fun_val();

void print_int(long v) {
	printf("%ld\n", v);
}

void print_block() {
	printf("\033[48;2;%d;%d;%dm", 255, 255, 255);
	printf(" \033[0m");
}

void print_space() {
	putc(' ', stdout);
}

int main() {
	long v = 30;

	printf("%ld\n", do_a_thing(66, 6, 500));
	printf("%ld\n", f());
	printf("%p\n", get_a_ptr());
	printf("%ld\n", deref_a_ptr(&v));
	printf("%p, %p\n", 0, offset_ptr(0));
	printf("%u\n", add_uints(30, 4));
	printf("%lu\n", up_cast(10));
	printf("%g\n", trunc_f64(40.6));

	printf("%d\n", *cast_ptr(&v));
	printf("%d\n", get_a_bool());

	printf("%d, %d\n", get_not(true), get_not(false));
	printf("%ld, %ld, %ld, %ld\n",
		are_ints_equal(20, 20),
		are_ints_equal(20, 3),
		are_ints_equal(12, 10),
		are_ints_equal(100, 100));
	
	print_hello();
	fizz_buzz(32);

	printf("%d\n", compare(12, 0));
	printf("%ld\n", get_a_fun_val());
}

int my_c_func(int a) {
	printf("Hello, from C. Value: %d.\n", a);
}
