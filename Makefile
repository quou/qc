ifndef config
  config=debug
endif

ifndef verbose
  silent = @
endif

.PHONY: clean

cc = clang
cxx = clang++
includes =
targetname = compiler
defines =
libs = -lLLVM-12
deps = 
srcdir = src
cstd = -std=c11
cxxstd = -std=c++20

ifeq ($(config),debug)
  targetdir = bin/debug
  target = $(targetdir)/$(targetname)
  lflags = -L/usr/lib64 -m64 -g
  defines += -Ddebug
  cflags = -MMD -MP -m64 -g $(cstd) $(includes) $(defines)
  cxxflags = -MMD -MP -m64 -g $(cxxstd) $(includes) $(defines)
  objdir = obj
endif

ifeq ($(config),release)
  targetdir = bin/release
  target = $(targetdir)/$(targetname)
  lflags = -L/usr/lib64 -m64 -s
  defined += -Dndebug
  cflags = -MMD -MP -m64 -O2 $(cstd) $(includes) $(defines)
  cxxflags = -MMD -MP -m64 -O2 $(cxxstd) $(includes) $(defines)
  objdir = obj/release
endif

all: $(target)
	@:

sources = $(wildcard $(srcdir)/*.c $(srcdir)/*/*.c)
sourcesxx = $(wildcard $(srcdir)/*.cpp $(srcdir)/*/*.cpp)
objects = $(sources:$(srcdir)/%.c=$(objdir)/%.o)
objectsxx = $(sourcesxx:$(srcdir)/%.cpp=$(objdir)/%.opp)

$(objects): | $(objdir)
$(objectsxx): | $(objdir)

$(objects): $(objdir)/%.o : $(srcdir)/%.c
	@echo $<
	$(silent) $(cc) $(cflags) -o "$@" -MF "$(@:%.o=%.d)" -c "$<"

$(objectsxx): $(objdir)/%.opp : $(srcdir)/%.cpp
	@echo $<
	$(silent) $(cxx) $(cxxflags) -o "$@" -MF "$(@:%.opp=%.dpp)" -c "$<"

$(target): $(objects) $(objectsxx) $(deps) | $(targetdir)
	@echo linking $(target)
	$(silent) $(cxx) -o "$@" $(objects) $(objectsxx) $(lflags) $(extlibs) $(libs)
	ctags -R

$(targetdir):
	$(silent) mkdir -p $(targetdir)

$(objdir):
	$(silent) mkdir -p $(objdir)

clean:
	$(silent) rm -rf obj
	$(silent) rm -rf bin
	$(silent) rm -rf tags

-include $(objects:%.o=%.d)
-include $(objectsxx:%.opp=%.dpp)
